﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using University.AppContext;

namespace University.WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeacherController : Controller
    {
        ApplicationDbContext db;
        public TeacherController(ApplicationDbContext context)
        {
            db = context;
        }

        [HttpGet]
        public IEnumerable<Teacher> Get()
        {
            var teachers = db.Teachers.ToArray();
            return teachers;
        }
    }
}