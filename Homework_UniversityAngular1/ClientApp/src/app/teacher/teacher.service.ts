import { Component, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Teacher } from './teacher';

@Injectable()
export class DataService {

  private url = "/api/teacher";

  constructor(private http: HttpClient) {
  }

  getTeachers() {
    return this.http.get(this.url);
  }
}
