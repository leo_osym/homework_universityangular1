import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { DataService } from './teacher.service';
import { Teacher } from './teacher';

@Component({
  selector: 'teacher',
  templateUrl: './teacher.component.html',
  providers: [DataService]
})
export class TeacherComponent implements OnInit {

  public teacher: Teacher;
  public teachers: Teacher[];
  tableMode: boolean = true;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.loadTeachers();
  }
  loadTeachers() {
    this.dataService.getTeachers()
      .subscribe((data: Teacher[]) => this.teachers = data);
  }
}

interface Teacherr {
    id: AAGUID,
    firstname: string,
    middlename: string,
    lastname: string,
    department?: string
}
