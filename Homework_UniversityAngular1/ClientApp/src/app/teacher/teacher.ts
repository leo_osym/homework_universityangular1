export class Teacher {
  constructor(
    public id: AAGUID,
    public firstname: string,
    public middlename: string,
    public lastname: string,
    public department?: string
  ) { }
}
